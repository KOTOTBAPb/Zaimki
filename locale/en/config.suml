locale: 'en'

header: true

pronouns:
    enabled: true
    route: 'pronouns'
    default: 'he'
    any: 'any'
    plurals: true
    honorifics: false
    multiple:
        name: 'Interchangeable forms'
        description: >
            Many nonbinary people use more than one form interchangeably
            and are fine with being called either of them.
            Some like when others alternate between those forms when talking about them.
        examples: ['he&she', 'he&they', 'she&they']
    null:
        description: >
            {/avoiding=Avoiding gendered forms} / {/no-pronouns=no pronouns} / {/null=null pronouns} /
            {/pronounless=pronounless} / {/nullpronominal=nullpronominal} / {/nameself=nameself}
        history: >
            Some people prefer not using any pronouns, instead being referred by name, initial,
            omitting pronouns with passive voice, or restructuring the sentence.
            See: {https://twitter.com/lypomania/status/1290274971642544128=lypomania's guide},
            {https://lgbta.wikia.org/wiki/Nullpronominal=Nullpronominal on LGBTA Wiki}
        morphemes:
            pronoun_subject: '#'
            pronoun_object: '#'
            possessive_determiner: '#''s'
            possessive_pronoun: '#''s'
            reflexive: '#self'
        examples: [':Andrea', ':S']
        template: 'Open one of the examples and simply replace the name/initial in the URL with your own.'
        routes: ['avoiding', 'no-pronouns', 'null', 'pronounless', 'nullpronominal', 'nameself']
        ideas:
            -
                header: 'Use names or initials instead of pronouns'
                normative: true
                examples:
                    - ['I talked to him yesterday', 'I talked to {/:Sky=Sky} yesterday']
                    - ['She is really beautiful', '{/:Soph=Soph} is really beautiful']
                    -
                        - 'Her graduation starts soon'
                        - '{/:J=J}''s graduation starts soon'
            -
                header: 'Passive voice'
                normative: true
                examples:
                    - ['He answered the phone', 'The phone was answered']
                    -
                        - 'Wen takes good care of her cat'
                        - 'Wen''s cat is well cared for'
            -
                header: 'Rephrasing the sentence (circumlocution)'
                normative: true
                examples:
                    - ['Lior did it all by himself', 'Lior did it all without any help']
                    - ['Gael talks in his sleep', 'Gael talks while sleeping']
            -
                header: 'Replacing a pronoun with a descriptive noun or phrase'
                normative: true
                examples:
                    - ['She landed the plane safely', 'The pilot landed the plane safely']
                    - ['This is Lea, she is into painting', 'This is Lea. My friend is into painting']
                    - ['She argues that…', 'The person who started this discussion argues that…']
            -
                header: 'Dropping pronouns'
                normative: true
                examples:
                    - ['Did you buy Tex her gift?', 'Did you buy Tex a gift?']
                    - ['Yes, I bought it for her. I will give it to her tomorrow.', 'Yes, I bought it. I will give it tomorrow.']
    emoji:
        description: 'Emojiself pronouns'
        history: '{https://lgbta.wikia.org/wiki/Emojiself_Pronouns=Emojiself} pronouns are intended for online communication and not supposed to be pronounced.'
        morphemes:
            pronoun_subject: '#'
            pronoun_object: '#'
            possessive_determiner: '#''s'
            possessive_pronoun: '#''s'
            reflexive: '#self'
        examples: ['💫', '💙']
        template: 'Open one of the examples and simply replace the emoji in the URL with your own.'
    mirror:
        route: 'mirror'
        name: 'Mirror pronouns / Mirrorpronominal'
        description: >
            A person who uses mirror pronouns wants to be referred to with the same pronouns as the person talking.
        example:
            - 'Person A uses mirror pronouns.'
            - 'Person B uses {/she=she/her}, so when she talks about person A, she uses “she/her” to refer to her.'
            - 'Person C uses {/ze=ze/hir} interchangeably with {/fae=fæ/fær}, so when ze talks about person A, fea uses either ze/hir or fæ/fær to refer to fær.'
    slashes: true
    others: 'Other pronouns'

pronunciation:
    enabled: true
    voices:
        GB:
            language: 'en-GB'
            voice: 'Emma'
            engine: 'neural'

sources:
    enabled: true
    route: 'sources'
    submit: true
    mergePronouns:
        they/them/themself: 'they'
    extraTypes: ['avoiding', 'nounself', 'mirror']

nouns:
    enabled: true
    route: 'dictionary'
    collapsable: false
    plurals: true
    pluralsRequired: false
    declension: false
    submit: true
    templates: true

community:
    route: 'terminology'

inclusive:
    enabled: false

terminology:
    enabled: true
    published: true
    categories:
        - 'sexual orientation'
        - 'romantic orientation'
        - 'tertiary orientation'
        - 'gender'
        - 'gender expression'
        - 'relationship model'
        - 'language'
        - 'attraction'
        - 'politics'
        - 'prejudice'

    route: 'terminology'

names:
    enabled: false

people:
    enabled: false

english:
    enabled: false

faq:
    enabled: true
    route: 'faq'

links:
    enabled: true
    split: true
    route: 'links'
    blogRoute: 'blog'
    links:
        -
            icon: 'globe-europe'
            url: 'https://pronoun.is/'
            headline: 'Pronoun.is'
            extra: '– inspiration for this website.'
        -
            icon: 'chart-pie'
            url: 'https://gendercensus.com/'
            headline: 'Gender Census'
            extra: '– annual survey about the language used by nonbinary people'
        -
            icon: 'comment-alt-edit'
            url: 'https://avris.it/blog/why-everyone-should-have-pronouns-in-their-bio'
            headline: 'Why all cis allies should have pronouns in their bio'
        -
            icon: 'book-open'
            url: 'https://www.merriam-webster.com/words-at-play/singular-nonbinary-they'
            headline: 'Merriam Webster on the singular “they” as a nonbinary pronoun'
        -
            icon: 'book-open'
            url: 'https://apastyle.apa.org/style-grammar-guidelines/grammar/singular-they'
            headline: 'Style and grammar guidelines regarding singular “they”'
            extra: '– American Psychological Association'
        -
            icon: 'book-open'
            url: 'https://apastyle.apa.org/blog/singular-they'
            headline: 'Welcome, singular “they”'
            extra: '– American Psychological Association'
        -
            icon: 'trophy'
            url: 'https://www.americandialect.org/2015-word-of-the-year-is-singular-they'
            headline: '2015 Word of the Year is singular “they”'
            extra: '– American Dialect Society'
        -
            icon: 'trophy'
            url: 'https://www.americandialect.org/2019-word-of-the-year-is-my-pronouns-word-of-the-decade-is-singular-they'
            headline: '2019 Word of the Year is “(My) Pronouns,” Word of the Decade is Singular “They”'
            extra: '– American Dialect Society'
        -
            icon: 'play-circle'
            url: 'https://www.youtube.com/watch?v=46ehrFk-gLk'
            headline: 'Gender Neutral Pronouns: They''re Here, Get Used To Them'
            extra: '– Tom Scott'
        -
            icon: 'book-open'
            url: 'https://blogs.illinois.edu/view/25/677177'
            headline: 'A Brief History of Singular “they”'
            extra: '– Dennis Baron'
        -
            icon: 'wikipedia-w'
            iconSet: 'b'
            url: 'https://lgbta.wikia.org/wiki/Neopronouns'
            headline: 'LGBTA Wiki on Neopronouns'
        -
            icon: 'book-open'
            url: 'https://scholar.google.com/scholar?hl=en&q=neopronouns'
            headline: 'Academic papers on neopronouns'
        -
            icon: 'newspaper'
            url: 'https://www.nytimes.com/2020/01/21/books/review/whats-your-pronoun-dennis-baron.html'
            headline: 'English’s Pronoun Problem Is Centuries Old'
            extra: '– The New York Times'
        -
            icon: 'user-edit'
            url: 'http://faculty.las.illinois.edu/debaron/'
            headline: 'Dennis Baron'
            extra: '– author of “What''s your pronoun?”, has been researching gender-neutrality for some 40 years now'
        -
            icon: 'book-open'
            url: 'https://ojs.lib.uwo.ca/index.php/wpl_clw/article/download/966/456'
            headline: 'Bucking the Linguistic Binary: Gender Neutral Language in English, Swedish, French, and German'
            extra: '– Levi C. R. Hord'
        -
            icon: 'twitch'
            iconSet: 'b'
            url: 'https://pronouns.alejo.io/'
            headline: 'Display pronouns in Twitch Chat'
        -
            icon: 'play-circle'
            url: 'https://youtu.be/0oYd4r6H9W0'
            headline: 'Personal Pronouns: Understanding He, She, They, and Beyond | BrainPOP'
        -
            icon: 'book-open'
            url: 'https://www.merriam-webster.com/words-at-play/words-were-watching-nibling'
            headline: 'Merriam Webster – Words We''re Watching: “Nibling”'

    academic: []
    mediaGuests: []
    mediaMentions: []
    recommended:
        -
            icon: 'cogs'
            url: 'https://omg.lol/'
            headline: 'OMG.LOL'
            extra: ' – domain name provider that uses Pronouns.Page API in user profiles'
        -
            icon: 'cogs'
            url: 'https://oengus.io/'
            headline: 'Oengus.io'
            extra: ' – uses Pronouns.Page API in user profiles'
        -
            icon: 'cogs'
            url: 'https://github.com/eramdam/BetterTweetDeck/releases/tag/4.5.2'
            headline: 'BetterTweetDeck'
            extra: ' – a TweetDeck extension that, among other things, displays user''s pronouns more prominently, with the help of our API'
        -
            icon: 'cogs'
            url: 'https://github.com/katacarbix/pronouns.js'
            headline: 'pronouns.js'
            extra: ' – a JavaScript library for parsing pronouns'
        -
            icon: 'cogs'
            url: 'https://pronoundb.org/'
            headline: 'PronounDB'
            extra: ' – browser extensions that display people''s pronouns on various platforms'
        -
            icon: 'sticky-note'
            url: 'https://leapsports.org/files/4225-Non-Binary%20Inclusion%20in%20sport%20Booklet.pdf'
            headline: 'Non-binary inclusion in sport'

    zine:
        enabled: false

    blog: true

contact:
    enabled: true
    route: 'contact'
    team:
        enabled: true
        route: 'team'

support:
    enabled: true

user:
    enabled: true
    route: 'account'
    termsRoute: 'terms'

profile:
    enabled: true
    editorEnabled: true
    defaultWords:
        -
            - '[no honorific]'
            - 'mx.'
            - 'mr.'
            - 'ms.'
            - 'sir'
            - 'ma''am'
            - 'madam'
            - 'sai'
            - 'mir'
            - 'shazam'
            - 'zam'
            - 'comrade'
        - ['person', 'man', 'woman', 'lady', 'dude', 'boy', 'girl', 'buddy', 'pal', 'bro', 'sis', 'sib']
        - ['pretty', 'handsome', 'cute', 'hot', 'sexy']
        - ['friend', 'partner', 'boyfriend', 'girlfriend', 'joyfriend', 'husband', 'wife', 'kissmate', 'darling', 'beloved', 'boo']
    flags:
        defaultPronoun: 'they'

calendar:
    enabled: true
    route: 'calendar'

census:
    enabled: false

blog:
    shortcuts:
        name: 'why-the-name'
        history: 'project-history'
        logo: 'new-logo'
        new-version: 'creating-new-language-version'

redirects:
    - { from: '^/blog/neutral-language-council$', to: '/team' }

api:
    examples:
        pronouns_all: ['/api/pronouns']
        pronouns_one:
            - '/api/pronouns/she/her'
            - '/api/pronouns/she/her?examples[]=Did%20you%20ask%20%7Bpronoun_object%7D%20to%20join%20us%3F'
        pronouns_banner: ['/api/banner/they.png']

        sources_all: ['/api/sources']
        sources_one: ['/api/sources/01ERQRCV0V4QVMNKCENF724A08']

        nouns_all: ['/api/nouns']
        nouns_search: ['/api/nouns/search/person']

        inclusive_all: ['/api/inclusive']
        inclusive_search: ['/api/inclusive/search/retard']

        terms_all: ['/api/terms']
        terms_search: ['/api/terms/search/agender']

        profile_get: ['/api/profile/get/andrea']

        calendar_today: ['/api/calendar/today']
        calendar_day: ['/api/calendar/2022-11-23']
