locale: 'sv'

header: true

pronouns:
    enabled: true
    route: 'pronomen'
    default: 'han'
    any: 'alla'
    plurals: true
    honorifics: false
    multiple:
        name: 'Omväxlande formulär'
        description: >
            Många ickebinära människor använder mer än en omväxlande formulär
            och är okej med att bli tilltalade med vilken som helst.
            gillar när andra växlar mellan de formulären när man pratar om dem.
        examples: ['han&hon', 'han&hen', 'hon&hen']
    null: false
    emoji: false
    slashes: true
    others: 'Andra pronomen'

pronunciation:
    enabled: true
    voices:
        SE:
            language: 'sv-SE'
            voice: 'Astrid'
            engine: 'standard'

sources:
    enabled: true
    route: 'kallor'
    submit: true
    mergePronouns: []
    extraTypes: []

nouns:
    enabled: true
    route: 'ordbok'
    collapsable: false
    plurals: true
    pluralsRequired: false
    declension: false
    submit: true
    templates: true

community:
    route: 'terminologi'

inclusive:
    enabled: false

terminology:
    enabled: true
    published: true
    categories:
        - 'sexuella läggningar'
        - 'romantiska läggningar'
        - 'tertiära läggningar'
        - 'kön'
        - 'könsuttryck'
        - 'relations modell'
        - 'språk'
        - 'attraktion'
        - 'politik'
        - 'fördomar'

    route: 'terminologi'

names:
    enabled: false

people:
    enabled: false

# optional, but would be nice to have
english:
    enabled: true
    route: 'english'
    pronounGroups:
        -
            name: 'Normative forms'
            description:
                - >
                    Because of the limitations of <language> grammar, or simply because they just prefer it that way,
                    many nonbinary people decide to simply use “he” ({/on=„on”}) or “she” ({/ona=„ona”})
                    – either the same as their gender assigned at birth or the opposite.
                    That doesn't make them any less nonbinary! Pronouns ≠ gender.
            table: {on: 'Masculine', ona: 'Feminine'}

faq:
    enabled: true
    route: 'faq'

links:
    enabled: false
    split: false
    route: 'lankar'
    blogRoute: 'blogg'
    links:
        -
            icon: 'globe-europe'
            url: 'https://pronoun.is/'
            headline: 'Pronoun.is'
            extra: '– inspirasjon for denne nettsiden.'
    academic: []
    mediaGuests: []
    mediaMentions: []
    recommended: []
    blog: false

contact:
    enabled: true
    route: 'kontakt'
    team:
        enabled: true
        route: 'team'

support:
    enabled: true

user:
    enabled: true
    route: 'konto'
    termsRoute: 'anvandarvillkor'

profile:
    enabled: true
    editorEnabled: true
    defaultWords:
        - ['person', 'man', 'kvinna', 'dam', 'snubbe', 'kille', 'tjej', 'kompis', 'bror', 'syster', 'syskon']
        - ['snygg', 'stilig', 'söt', 'het', 'sexig']
        - ['vän', 'partner', 'pojkvän', 'flickvän', 'make', 'fru', 'pusskamrat', 'käresta', 'älskling']
    flags:
        defaultPronoun: 'hen'

calendar:
    enabled: true
    route: 'kalender'

census:
    enabled: false

redirects: []

api: ~
